![image](https://i.imgur.com/FlfwMMi.png)

# Reportes DevExpress ASP MVC API y Visor de Documentos Web en Angular

**Requisitos**
Antes que nada debemos asegurarnos contar con los siguientes requisitos:

- [**DevExpress 17.2.7**](https://www.devexpress.com/Home/try.xml)
- **Visual Studio 2015** o posterior
- [**Nodejs 8.11.1** o posterior](https://nodejs.org/es/download/) 
- **npm 5.6.0** o posterior (Incluido al instalar Nodejs)
- [**@angular/cli 1.6.5**](https://cli.angular.io/)


## Instrucciones para ejecutar los proyectos
Al clonar el repositorio, nos encontraremos con dos proyectos `ReportesClienteAngular` para el cliente y `ReportesServidorMvcApi` para el lado del servidor.

- Abriremos primero el proyecto `ReportesServidorMvcApi`. Ejecutaremos `ReportesMvcApi.sln` con Visual estudio, compilar y ejecutar.
- Para correr el cliente abrir una consola dentro del directorio `ReportesClienteAngular`.
- Usar el comando `npm install` para instalar los modulos necesarios via npm.
- Una vez instalados los modulos ejecutar el comando `npm start` para ejecutar la aplicación

Al ejecutar `npm start` navegar a la url `http://localhost:4200/`. Podremos ver la aplicación corriendo de forma correcta.

### How-To

La aplicacion esta basada en el modelo **cliente-servidor** para esto tendremos dos soluciones las cuales se unificaran para crear una sola.

Para la parte del servidor se utilizara un proyecto **ASP MVC WebApi**, como IDE usaremos **Visual Studio 2015**, aunque no debe de existir problema alguno si se utiliza alguna version posterior como **VS 2017 Community** etc.

En la parte del cliente se utilizará Angular para todo el desarrollo del front

La finalidad del proyecto es integrar el reporteador de DevExpress para relaizar reportes y que la logica se encuentre en el lado del servidor, y utilicemos angular para poder visualizar estos reportes.

#### Requerimientos

Para realizar el tutorial correctamente

- Se esta utilizando DevExpress en su versión **_17.2.7 (23-MARCH-2018)_**  https://www.devexpress.com/support/whatsnew/
- Antes de iniciar cualquier cosa, es necesario que instalemos DevExpress para poder realizar este tutorial.
- Se da por entendido que ya se tienen instaladas las herramientas mencionadas al inicio de esta documentacion para el desarrollo de angular

**Back-End**

Crear un proyecto nuevo asp MVC web Api.

![JPG](https://i.imgur.com/FghkJZR.png)

Una vez que el proyecto este creado, crearemos la carpeta `Reports` en el directorio raíz y dentro del directorio `Reports` crearemos otros directorios, `Reportes` y `ReportsDataSource` está será la estructura que manejaremos.

![JPG](https://i.imgur.com/j2IvvP5.png)

### Proceso de creación y binding de un reporte

Para bindear un objeto vinculante al reporte de devexpress es necesario preparar un **Origen de datos**  Para ello DevExpress facilita un ensamblado [DevExpress.DataAccess](https://documentation.devexpress.com/CoreLibraries/DevExpress.DataAccess.namespace) que ayuda a realizar esta tarea así que como primer paso hay que agregar esta referencia al proyecto.

Lo primero será crear una clase dentro de la carpete Models la cual se usará para crear el objeto que recibiremos. Esta clase cuenta con una clase llamada detalle con la cual aremos un reporte enfocado en Master-detail.

**Producto.cs**

```cs
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ReportesMvcApi.Models
{
    public class Producto
    {
        private int pk = 0;
        private string codigo = "Sin definir";
        private string nombre = "Sin definir";
        public int cantidad;
        public string descripcion = "sin definir";
        public string comentario = "sin definir";
        public List<DetalleProducto> detalle { get; set; }

        [DisplayName("Pk")]
        public int Pk
        {
            get { return pk; }
            set { pk = value; }
        }

        [DisplayName("Codigo")]
        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        [DisplayName("Nombre")]
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        [DisplayName("Cantidad")]
        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }

        [DisplayName("Descripcion")]
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        [DisplayName("Comentario")]
        public string Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }
        public Producto(int pk, string codigo, string nombre, int cantidad, string descripcion, string comentario)
        {
            this.pk = pk;
            this.codigo = codigo;
            this.nombre = nombre;
            this.cantidad = cantidad;
            this.descripcion = descripcion;
            this.comentario = comentario;
            detalle = new List<DetalleProducto>();
            detalle.Add(new DetalleProducto { descripcion = "Hola mundo uno" });
            detalle.Add(new DetalleProducto { descripcion = "Hola mundo dos" });
            detalle.Add(new DetalleProducto { descripcion = "Hola mundo tres" });

        }
    }
    public class DetalleProducto
    {
        public string descripcion { get; set; }
        public DateTime fecha { get { return DateTime.Now; } }
    }
}

```

Lo siguiente sera crear la clase que le indicara a DevExpress que es un **origen de datos** y poder hacer la logica interna que nos devuelva el objeto que pediremos desde el cliente

En esta parte se hara uso de los tags [[HighlightedClass]](https://documentation.devexpress.com/CoreLibraries/DevExpress.DataAccess.ObjectBinding.HighlightedClassAttribute.class) y [[HighlightedMember]](https://documentation.devexpress.com/CoreLibraries/DevExpress.DataAccess.ObjectBinding.HighlightedMemberAttribute.class) los cuales le indicaran a DevExpress que esta clase tiene un origen de datos a través de un miembro. Es necesario utilizar el Namespace [DevExpress.DataAccess.ObjectBinding](https://documentation.devexpress.com/CoreLibraries/DevExpress.DataAccess.ObjectBinding.namespace)

Dentro de la carpeta ReportsDataSource crearemos la clase **OdsProductos**, La función de esta clase será recibir un valor en este caso un Pk desde el cliente el cual servira que objeto mostrar en el reporte.

**OdsProductos.cs**

```cs
using ReportesMvcApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.DataAccess.ObjectBinding;
using System.Web;

namespace ReportesMvcApi.Reports.ReportsDataSource
{
    [HighlightedClass]
    public class OdsProductos : List<Producto>
    {
        [HighlightedMember]
        public OdsProductos(int value)
        {            
            var producto_uno = new Producto(1,"4589", "zapatos", 30, "zapatos azules", "Descripcion del producto uno" );
            var producto_dos = new Producto(2,"0001", "gorras", 100, "gorras rojas", "Descripcion del producto dos");

            if (value==1)
            {
                this.Add(producto_uno);
            }
            else
            {
                if (value == 2)
                {
                    this.Add(producto_dos);
                }
            }
            
            
        }
    }
}
``` 
Por ultimo es necesario agregar información del ensamblado al **AssemblyInfo.cs** esto para que al momento de usar el wizard y crear un reporte nos aparesca en las opciones de objeto vinculante la clase que acabamos de crear.

**AssemblyInfo.cs**
```cs
using DevExpress.DataAccess.ObjectBinding;
...
[assembly: HighlightedAssembly]
...
``` 
### Crear reporte 

Seleccionamos la carpeta Reportes precionamos ``Ctrl + Shift + A`` para agregar un nuevo elemento

![image](https://i.imgur.com/RfwZjhq.png)

En la pantalla que se muestra, seleccionar el apartado de Reporting en la parte izquierda, posteriormente seleccionar DevExpress v17.2 Report wizard, finalmente se le asigna un nombre respetando la nomenclatura que se establece en la documentacion [Doc Implementar un reporte en DevExpress](https://gitlab.com/krknsolutions/gadai/administrativo/uploads/8cf3cefdcf784663e2b7c832a8c57757/documentacion_Reportes.pdf) dar clic en agregar.

![image](/uploads/be16576b21edaa95b5c3e88c268df952/image.png)

Sleccionar **Data-Bound Report** Para hacer una plantilla del reporte automaticamente. (Opcionalmente se puede elegir cualquiera de las otras opciones para personalizar el diseño del reporte). Click en Siguiente.

![image](/uploads/d3dd733de8c4b6db1e6d1bfef054f949/image.png)

Seleccionar el tipo de fuente de datos **Objeto Vinculante** y dar click en siguiente.

![image](/uploads/72f6ce4c5c64f18d4c97853c5a3f088c/image.png)

Se debe seleccionar el ensamblado donde se encuentra la definicion del tipo de clase del origen de datos, en este caso se encuentra en **WA_REPORT_MANAGER** dar doble clic en el ensamblado y posteriormente en la clase donde se encuentra la definicion del origen de datos.

![Alt Text](https://i.imgur.com/C8xKjZE.gif)

Seleccionar recuperar datos reales, click en siguiente.

![image](/uploads/d7c4f31b0887aab039f91fcd12d29b7d/image.png)

Las siguientes configuraciones del wizard se pueden establecer dependiendo de los campos que se quieran agregar al reporte, agrupar los datos entre otras cosas
Para este caso en la parte de expression seleccionaremos la casilla y haremos click en la opcion external parameter donde indicaremos que el constructor se inicializara con un parametro externo. 

![gif](https://i.imgur.com/eL69cZO.gif)

Al finalizar el asistente se puede ver el reporte en vista de diseño y obtener vistas previas sin necesidad de ejecutar la aplicacion.

![gif](https://i.imgur.com/fgt66qs.gif)

### Control de reportes y peticiones al servidor

Dentro de la carpeta `Reports` crear la clase **ReportStorage**, esta hereda de [**ReportStorageWebExtension**](https://documentation.devexpress.com/XtraReports/DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension.class) la cual se esta implementando un almacen de reportes personalizado, los reportes se almacenan en un diccionario para facilitar la llamada de cada reporte a través de su key. Ademas esta clase nos permite administrar el manejo y devoluciones de los reportes.

**ReportStorage.cs**

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.Extensions;
using System.IO;
using ReportesMvcApi.Reports.Reportes;

namespace ReportesMvcApi.Reports
{
    public class ReportStorage : ReportStorageWebExtension
    {
        public Dictionary<string, XtraReport> Reports = new Dictionary<string, XtraReport>();

        public ReportStorage()
        {
            Reports.Add("Prueba_Uno", new Reporte_Prueba_Uno());            
        }

        public override bool CanSetData(string url)
        {
            return true;
        }

        public override byte[] GetData(string url)
        {
            char[] delimit = new char[] { '/' };
            List<string> uri = new List<string>();

            foreach (string substr in url.Split(delimit))
            {
                uri.Add(substr);
            }

            var report = Reports[uri[0]];
            using (MemoryStream stream = new MemoryStream())
            {
                report.Parameters["Pk"].Value = int.Parse(uri[1]);
                report.Parameters["Pk"].Visible = false;
                report.SaveLayoutToXml(stream);
                return stream.ToArray();
            }
        }

        public override Dictionary<string, string> GetUrls()
        {
            return Reports.ToDictionary(x => x.Key, y => y.Key);
        }

        public override void SetData(XtraReport report, string url)
        {
            if (Reports.ContainsKey(url))
            {
                Reports[url] = report;
            }
            else
            {
                Reports.Add(url, report);
            }
        }

        public override string SetNewData(XtraReport report, string defaultUrl)
        {
            SetData(report, defaultUrl);
            return defaultUrl;
        }

        public override bool IsValidUrl(string url)
        {
            return true;
        }
    }
}
```

Es importante observar que estamos usando el metodo **GetData()** para realizar la logica que nos permitausar la url que venga del servidor y separar el nombre del reporte y el valor del parametro recibido.

Si miramos el código
```csharp
public override byte[] GetData(string url)
        {
            char[] delimit = new char[] { '/' };
            List<string> uri = new List<string>();

            foreach (string substr in url.Split(delimit))
            {
                uri.Add(substr);
            }

            var report = Reports[uri[0]];
            using (MemoryStream stream = new MemoryStream())
            {
                report.Parameters["Pk"].Value = int.Parse(uri[1]);
                report.Parameters["Pk"].Visible = false;
                report.SaveLayoutToXml(stream);
                return stream.ToArray();
            }
        }
```
Notaremos que el metodo trae consigo la cadena url. Supongamos que el valor que recibimos es `MiReporte/55` donde `MiReporte` es el nombre del reporte y `55` es la clave primaria del objeto. Tendremos que separar la cadena por el delimitador `/` y aqui es donde lo hacemos

Tendremos que nuestra lista llamada uri en su indice **uri[0]** tiene el nombre del reporte y el indice **uri[1]** tiene el pk que recibimos.

```csharp
    report.Parameters["Pk"].Value = int.Parse(uri[1]);
    report.Parameters["Pk"].Visible = false;
```
En estas lineas estamos indicando que pasaremos el contenido de **uri[1]** al parametro del reporte que a su vez se ira directamente al constructor de nuestra clase **OdsProductos.cs**.

Posteriormente necesitamos un controlador que reciba las peticiones del cliente donde le indicará que reporte es el que quiere que devuelva. Así que iremos a la carpeta controllers y agregaremos un controlador llamado `WebDocumentViewerController` 

**WebDocumentViewerController** Este Controlador hereda de [**WebDocumentViewerApiController**](https://documentation.devexpress.com/AspNet/DevExpress.Web.Mvc.Controllers.WebDocumentViewerApiController.class) la cual es un ensamblado de DevExpress permite procesar las peticiones de reportes que haga el cliente en este caso el WebDocumentViewer que se implementa en angular.

**GetReports()** Permite obtener un listado de los reportes que se tienen almacenados en el diccionario estos se devuelven en formato JSON

**WebDocumentViewerController.cs**

```csharp

using DevExpress.Web.Mvc.Controllers;
using DevExpress.XtraReports.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportesMvcApi.Controllers
{
    public class WebDocumentViewerController : WebDocumentViewerApiController
    {
        /// <summary>
        /// Acción que procesa la peticion desde el Web Document Viewer
        /// </summary>
        /// <returns>System.Web.ActionResult objeto resultante de la acción</returns>
        public override ActionResult Invoke()
        {
            var result = base.Invoke();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            return result;
        }

        /// <summary>
        /// Acción que obtiene un listado de los reportes almacenados
        /// </summary>
        /// <returns>Json de los reportes almacenados en MyReportStorage</returns>
        [HttpGet]
        public ActionResult GetReports()
        {
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            var result = new JsonResult
            {
                Data = ReportStorageWebService.GetUrls().ToArray()
            };
            return result;
        }
    }
}
```

Es necesario agregar la siguiente linea al **Global.asax** para inicializar la clase MyReportStorage y se puedan almacenar los reportes al iniciar la aplicación

**Global.asax.cs**
```cs
//Se inicializa  la clase MyreportStorage  para guardar los reportes existentes en un diccionario
ReportStorageWebExtension.RegisterExtensionGlobal(new MyReportStorage());

```



**FRONT-END**

Para la parte del front, trabajaremos con las dependencias [devexpress-reporting](https://www.npmjs.com/package/devexpress-reporting) y [devextreme](https://www.npmjs.com/package/devextreme).

Antes que nada, se requiere agregar estas dependencias a nuestro package.json

**package.json**

```json
    "devexpress-reporting": "~17.2.5",
    "devextreme": "~17.2.5",
    "globalize": "~1.3.0",
    "cldrjs": "~0.5.0",
    "knockout": "~3.4.2",
    "jquery": "~3.1.1",
    "jquery-ui": "~1.12.1"
```
 es necesario generar el siguiente componente y modificar los archivos que se mencionan a continuación.

**report-viewer.component.ts** el cual nos permite realizar la petición de cierto reporte al servidor

```ts
import { Component, ViewChild, AfterViewInit, Renderer2, Input, ElementRef } from '@angular/core';
import * as ko from "knockout";
import { Html } from "devexpress-reporting/dx-web-document-viewer";

@Component({
  selector: 'report-viewer',
  templateUrl: './report-viewer.component.html',
  styleUrls: ['./report-viewer.component.css']
})
export class ReportViewerComponent implements AfterViewInit {

  koReportUrl = ko.observable(null);
  _reportUrl;

  @ViewChild('scripts')
  scripts: ElementRef;

  @ViewChild("control")
  control: ElementRef

  constructor(private renderer: Renderer2) { }

  ngAfterViewInit() {
    const reportUrl = this.koReportUrl,
      host = 'http://localhost:55041/',
      container = this.renderer.createElement("div");
    container.innerHTML = Html;
    this.renderer.appendChild(this.scripts.nativeElement, container);
    ko.applyBindings({
      reportUrl, // La URL del reporte que sera mostrado en el Document Viewer.
      requestOptions: { 
        host, //  URI del back-end.
        invokeAction: "/WebDocumentViewer/Invoke" // La URI del controlador que procesa la peticion.
      }
    }, this.control.nativeElement);
  }

  @Input()
  set reportUrl(reportUrl: string) {
    this._reportUrl = reportUrl;
    this.koReportUrl(reportUrl);
  }
  get reportUrl() {
    return this._reportUrl;
  }
}

```


**report-viewer.component.html**

```html
<div #scripts></div>
<div #control style="width:100%; height: 1000px">
  <div data-bind="dxReportViewer: $data"></div>
</div>
```

**app.component.ts**

```ts
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  reportUrl = "Prueba_Uno/1"; // Nombre del reporte que se desea llamar /despues del slash estamos mandando un parametro.
}
```
**app.component.html**

```html
<div style="width:100%; height:100%">
  <report-viewer [reportUrl]="reportUrl"></report-viewer>
</div>
```

**webpack.config.js**
Se requiere añadir lo siguiente al archivo de configuracion de webpack

```js

const alias = rxPaths();
alias["globalize$"] = path.resolve(__dirname, "node_modules/globalize/dist/globalize.js");
alias["globalize"] = path.resolve(__dirname, "node_modules/globalize/dist/globalize");
alias["cldr$"] = path.resolve(__dirname, "node_modules/cldrjs/dist/cldr.js");
alias["cldr"] = path.resolve(__dirname, "node_modules/cldrjs/dist/cldr");

module.exports = {
  "resolve": {
    ...
    "alias":alias,
    ...
  },
```

**styles.css**

```css
@import url("../node_modules/devextreme/dist/css/dx.common.css");
@import url("../node_modules/devextreme/dist/css/dx.light.css");
@import url("../node_modules/devexpress-reporting/css/web-document-viewer-light.min.css");
```

### Resultado

Finalmente este es el resultado al llamar al reporte desde el cliente web.

![image](https://i.imgur.com/FVRu6zA.png)



### Posibles Errores

Durante el proceso de realización se tuvieron algunas situaciones imprevistas, en las cuales se desconocía el por que del error.

El error que se produce es que al crear y establecer el origen de datos en el proyecto este no aparece en el wizard y no es posible bindear un objeto vinculante por ningun metodo, simplemente el wizard no nos proporciona ninguna opción valida para el *origen de datos* y nos encontraremos con esta pantalla.

![image](/uploads/72c4b196cc23f09f5e4323dbbf3b64e4/image.png)

Se detecto, que el problema se encuentra al agregar la referencia **DevExpress.Web.Mvc5** misma que es utilizada para procesar las peticiones de reportes que haga el cliente. 

![image](/uploads/e71ecf3afe63d931cc6711f9ecbdc7ec/image.png)

La solución momentánea que se pudo aplicar es quitar esa referencia cuando se agregue un nuevo reporte, posterior a eso comentar el controlador en el que se esta usando ese ensamblado y recompilar la solución. 

![gif](https://i.imgur.com/cRStDbw.gif)

Al realizar esto el wizard funcionara de manera correcta, una vez implementado el reporte revertir estos cambios aplicados y todo estará funcionando correctamente.

![gif](https://i.imgur.com/OGCIDPi.gif)

Se sigue buscando una solución mas adecuada para resolver este problema.

**Referencias**
--------------------------------------

**Back-end**
* [Implementando DocumentWebApi del lado del servidor](https://documentation.devexpress.com/XtraReports/118597/Creating-End-User-Reporting-Applications/Web-Reporting/Using-Reporting-Controls-in-JS/Creating-a-Server-Side-for-the-Web-Document-Viewer)

**Enlace de datos a un reporte**

* [Doc Implementar reporte en DevExpress](/uploads/8cf3cefdcf784663e2b7c832a8c57757/documentacion_Reportes.pdf)
* [Bindear un report a un archivo xml](https://documentation.devexpress.com/XtraReports/5154/Creating-Reports-in-Visual-Studio/Detailed-Guide-to-DevExpress-Reporting/Providing-Data-to-Reports/Tutorials-and-Code-Examples/Bind-a-Report-to-an-XML-File)
* [Bindear reporte a un archivo XML en tiempo de ejecución](https://documentation.devexpress.com/XtraReports/5153/Creating-Reports-in-Visual-Studio/Detailed-Guide-to-DevExpress-Reporting/Providing-Data-to-Reports/Tutorials-and-Code-Examples/Bind-a-Report-to-an-XML-File-Runtime-Sample)
* [Bindear reporte a un Object Data Source](https://documentation.devexpress.com/XtraReports/17784/Creating-Reports-in-Visual-Studio/Detailed-Guide-to-DevExpress-Reporting/Providing-Data-to-Reports/Tutorials-and-Code-Examples/Bind-a-Report-to-an-Object-Data-Source)

**Front-End**

* [HTML Document Viewer](https://documentation.devexpress.com/XtraReports/17738/Creating-End-User-Reporting-Applications/Web-Reporting/Document-Viewer/HTML5-Document-Viewer)
* [Implementar un visor de reportes en angular](https://documentation.devexpress.com/XtraReports/119430/Creating-End-User-Reporting-Applications/Web-Reporting/Using-Reporting-Controls-in-JS/Document-Viewer-Integration-in-Angular2)
* [Q-A ¿Como implementar un reporte en Angular 2?](https://www.devexpress.com/Support/Center/Question/Details/T360117/how-to-integrate-devexpress-reporting-components-into-an-asp-net-core-a-k-a-vnext-asp)

* [Proyecto Ejemplo Integración Angular](/uploads/2b4e1cea62ee93bd61863dd27a038f9d/GH499.zip)

* [Proyecto para pruebas Report_manager_Front ](https://gitlab.com/odprz/devexpress-document-viewer-angular)