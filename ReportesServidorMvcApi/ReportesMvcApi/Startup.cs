﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReportesMvcApi.Startup))]
namespace ReportesMvcApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
