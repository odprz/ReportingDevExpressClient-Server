﻿using DevExpress.XtraReports.Web.Extensions;
using ReportesMvcApi.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ReportesMvcApi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Se inicializa  la clase MyreportStorage  para guardar los reportes existentes en un diccionario
            ReportStorageWebExtension.RegisterExtensionGlobal(new ReportStorage());
        }
    }
}
