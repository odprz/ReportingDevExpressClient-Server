﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ReportesMvcApi.Models
{
    public class Producto
    {
        private int pk = 0;
        private string codigo = "Sin definir";
        private string nombre = "Sin definir";
        public int cantidad;
        public string descripcion = "sin definir";
        public string comentario = "sin definir";
        public List<DetalleProducto> detalle { get; set; }

        [DisplayName("Pk")]
        public int Pk
        {
            get { return pk; }
            set { pk = value; }
        }

        [DisplayName("Codigo")]
        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        [DisplayName("Nombre")]
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        [DisplayName("Cantidad")]
        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }

        [DisplayName("Descripcion")]
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        [DisplayName("Comentario")]
        public string Comentario
        {
            get { return comentario; }
            set { comentario = value; }
        }
        public Producto(int pk, string codigo, string nombre, int cantidad, string descripcion, string comentario)
        {
            this.pk = pk;
            this.codigo = codigo;
            this.nombre = nombre;
            this.cantidad = cantidad;
            this.descripcion = descripcion;
            this.comentario = comentario;
            detalle = new List<DetalleProducto>();
            detalle.Add(new DetalleProducto { descripcion = "Hola mundo uno" });
            detalle.Add(new DetalleProducto { descripcion = "Hola mundo dos" });
            detalle.Add(new DetalleProducto { descripcion = "Hola mundo tres" });

        }
    }
    public class DetalleProducto
    {
        public string descripcion { get; set; }
        public DateTime fecha { get { return DateTime.Now; } }
    }
}