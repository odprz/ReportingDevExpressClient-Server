﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.Extensions;
using System.IO;
using ReportesMvcApi.Reports.Reportes;

namespace ReportesMvcApi.Reports
{
    public class ReportStorage : ReportStorageWebExtension
    {
        public Dictionary<string, XtraReport> Reports = new Dictionary<string, XtraReport>();

        public ReportStorage()
        {
            Reports.Add("Prueba_Uno", new Reporte_Prueba_Uno());            
        }

        public override bool CanSetData(string url)
        {
            return true;
        }

        public override byte[] GetData(string url)
        {
            char[] delimit = new char[] { '/' };
            List<string> uri = new List<string>();

            foreach (string substr in url.Split(delimit))
            {
                uri.Add(substr);
            }

            var report = Reports[uri[0]];
            using (MemoryStream stream = new MemoryStream())
            {
                report.Parameters["Pk"].Value = int.Parse(uri[1]);
                report.Parameters["Pk"].Visible = false;
                report.SaveLayoutToXml(stream);
                return stream.ToArray();
            }
        }

        public override Dictionary<string, string> GetUrls()
        {
            return Reports.ToDictionary(x => x.Key, y => y.Key);
        }

        public override void SetData(XtraReport report, string url)
        {
            if (Reports.ContainsKey(url))
            {
                Reports[url] = report;
            }
            else
            {
                Reports.Add(url, report);
            }
        }

        public override string SetNewData(XtraReport report, string defaultUrl)
        {
            SetData(report, defaultUrl);
            return defaultUrl;
        }

        public override bool IsValidUrl(string url)
        {
            return true;
        }
    }
}
