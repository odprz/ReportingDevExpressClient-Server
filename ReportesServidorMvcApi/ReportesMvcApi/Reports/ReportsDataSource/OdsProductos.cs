﻿using ReportesMvcApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.DataAccess.ObjectBinding;
using System.Web;

namespace ReportesMvcApi.Reports.ReportsDataSource
{
    [HighlightedClass]
    public class OdsProductos : List<Producto>
    {
        [HighlightedMember]
        public OdsProductos(int value)
        {            
            var producto_uno = new Producto(1,"4589", "zapatos", 30, "zapatos azules", "Descripcion del producto uno" );
            var producto_dos = new Producto(2,"0001", "gorras", 100, "gorras rojas", "Descripcion del producto dos");

            if (value==1)
            {
                this.Add(producto_uno);
            }
            else
            {
                if (value == 2)
                {
                    this.Add(producto_dos);
                }
            }
            
            
        }
    }
}