﻿using DevExpress.Web.Mvc.Controllers;
using DevExpress.XtraReports.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReportesMvcApi.Controllers
{
    public class WebDocumentViewerController : WebDocumentViewerApiController
    {
        /// <summary>
        /// Acción que procesa la peticion desde el Web Document Viewer
        /// </summary>
        /// <returns>System.Web.ActionResult objeto resultante de la acción</returns>
        public override ActionResult Invoke()
        {
            var result = base.Invoke();
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            return result;
        }

        /// <summary>
        /// Acción que obtiene un listado de los reportes almacenados
        /// </summary>
        /// <returns>Json de los reportes almacenados en MyReportStorage</returns>
        [HttpGet]
        public ActionResult GetReports()
        {
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            var result = new JsonResult
            {
                Data = ReportStorageWebService.GetUrls().ToArray()
            };
            return result;
        }
    }
}